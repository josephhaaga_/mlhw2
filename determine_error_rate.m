function [ output_args ] = determine_error_rate( classifier_labels, real_labels )
%Determine_Error_Rate Determine error rate of Classifier
%   Checks the label's our classifier assigned to the testData vs. the
%   actual testLabels value and return error rate

% a matrix with 1's where our classifier correctly labeled an input 
output_args= (1-(sum(classifier_labels==real_labels)/size(real_labels,1)));

end


