function [ output_args ] = generate_bayesian( data, labels )
%Generate_Bayesian Return a generative classifier
%   Data and corresponding labels are iterated through while probability is
%   updated.
% data = <198 * 22 double>
% labels = <198 * 1 double>  1 OR 0

a = find(labels==0);
b = find(labels==1);

a_records = data(a,:);
b_records = data(b,:);


%initialize p=0.5 and n=2
p_prior = ones(1,size(data,2));
p_prior = p_prior./2;
n=2;

a_probabilities = ones(1,size(data,2));
b_probabilities = ones(1,size(data,2));

% NUMBER OF POINTS W/ Xi = 1 for classes '0' and '1'
x_i_A = sum(a_records,1);
x_i_B = sum(b_records,1);


%for each record that's labeled a '0'
for a_count=1:size(a)
    % for each feature
    for m=1:size(data,2)
        % calculate P_i
        a_probabilities(m) = ((x_i_A(m)+(n*p_prior(m)))/(size(a_records,1) + n ));
        p_prior(m) = a_probabilities(m);
    end
end

p_prior = ones(1,size(data,2));
p_prior = p_prior./2;


%for each record that's labeled a '0'
for b_count=1:size(b)
    % for each feature
    for z=1:size(data,2)
        % calculate P_i
        b_probabilities(z) = ((x_i_B(z)+(n*p_prior(z)))/(size(b_records,1) + n ));
        p_prior(z) = b_probabilities(z);
    end
end



output_args = [a_probabilities;b_probabilities];


