function [ output_args ] = classify_bayesian( classifier, data )
%Classify_Bayesian Use input classifier struct to classify input data
%   Detailed explanation goes here

%
temp = ones(size(data,1),2);

classified = zeros(size(data,1),1);

for z=1:size(data,1)
    for n=1:size(data,2)
        temp(z,1) = temp(z,1) * ((classifier(1,n).^data(z,n))*((1-classifier(1,n)).^(1-data(z,n))));
        temp(z,2) = temp(z,2) * ((classifier(2,n).^data(z,n))*((1-classifier(2,n)).^(1-data(z,n))));
    end
    if temp(z,1)>temp(z,2)
        classified(z) = 0;
    else
        classified(z) = 1;
    end
end



output_args = classified;

